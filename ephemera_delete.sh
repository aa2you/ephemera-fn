#!/usr/bin/env bash

# Kubeless
export RELEASE=v1.0.0-alpha.7
#export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
kubectl delete ns kubeless 
kubectl delete -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml

# Nginx controller 
kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/baremetal/service-nodeport.yaml

# Kafka and Zookeeper
kubectl delete -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kafka-zookeeper-$RELEASE.yaml
kubectl delete -f kafka_zookeeper_persistent_storage.yaml 

# Mosca  (MQTT/Kafka)
kubectl delete -f mosca.yaml

# VerneMQ (MQTT/Webhook)
kubectl delete -f vernemq.yaml
