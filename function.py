from __future__ import print_function
import json as jsonlib
import os
import datetime
from base64 import b64decode
from pymongo import MongoClient
from paho.mqtt.client import Client as MqttClient

from kafka import KafkaProducer
from kafka.errors import KafkaError


MQTT_HOST = "iotg"
MQTT_PORT = 1883
MQTT_USERNAME = "ephemera"
MQTT_PASSWORD = "ephemera"

KAFKA_HOST = "kafka.kubeless"
KAFKA_PORT = "9092"

PREDICTIONS_TOPIC = "fn-predictions"
DENSITY_TOPIC = "fn-density"

MQTT_LOCATIONS_TOPIC = "faas-locations"
MQTT_FUTURE_LOCATIONS_TOPIC = "faas-future-locations"

MQTT_PROTO = "mqtt"
KAFKA_PROTO = "kafka"


# MONGODB_HOST = "localhost"
MONGODB_HOST = "mongo"
MONGODB_PORT = 27017
MONGODB_DATABASE = "faas_demo"
MONGODB_COLLECTION = "car_events"
MONGODB_CONNECTION_URL = "mongodb://" + MONGODB_HOST + ":" + str(MONGODB_PORT)


class FunctionException(Exception):
	pass


class Queue:

	def __init__(self):

		try:
			self.publish_proto = os.environ['PUBLISH_PROTO']
		except KeyError:
			self.publish_proto = KAFKA_PROTO

		if self.publish_proto == MQTT_PROTO:
			try:
				self.mqtt_host = os.environ['MQTT_HOST']
			except KeyError:
				self.mqtt_host = MQTT_HOST
			try:
				self.mqtt_port = os.environ['MQTT_PORT']
			except KeyError:
				self.mqtt_port = MQTT_PORT
			try:
				self.mqtt_username = os.environ['MQTT_USERNAME']
			except KeyError:
				self.mqtt_username = MQTT_USERNAME
			try:
				self.mqtt_password = os.environ['MQTT_PASSWORD']
			except KeyError:
				self.mqtt_password = MQTT_PASSWORD

		else:
			try:
				self.kafka_host = os.environ['KAFKA_HOST']
			except KeyError:
				self.kafka_host = KAFKA_HOST
			try:
				self.kafka_port = os.environ['KAFKA_PORT']
			except KeyError:
 				self.kafka_port = KAFKA_PORT

		try:
			self.predictions_topic = os.environ['PREDICTIONS_TOPIC']
		except KeyError:
			self.predictions_topic = PREDICTIONS_TOPIC
		try:
			self.density_topic = os.environ['DENSITY_TOPIC']
		except KeyError:
			self.density_topic = DENSITY_TOPIC

	def connect(self, type):

		if self.publish_proto == KAFKA_PROTO:
			try:
				self.kafka_producer = KafkaProducer(bootstrap_servers=[self.kafka_host+':'+self.kafka_port], 
						client_id=type,
						value_serializer=lambda m: jsonlib.dumps(m).encode('ascii'))
				print('Kafka connected to %s:%s' % (self.kafka_host, self.kafka_port))
			except Exception as e:
 				print('Cannot connect to Kafka %s' % str(e))
 				raise FunctionException

		if self.publish_proto == MQTT_PROTO:
			try:
				self.mqtt_client = MqttClient(type)
				self.mqtt_client.username_pw_set(self.mqtt_username, self.mqtt_password)
				self.mqtt_client.connect (self.mqtt_host, self.mqtt_port, 60)
				print('MQTT connected to %s:%s (u:%s,p:%s)' % (self.mqtt_host, self.mqtt_port, self.mqtt_username, self.mqtt_password))
			except Exception as e:
				print('MQTT connection failed %s' % str(e))
				raise FunctionException
	
	def publish_prediction(self, event):
		print("{}_car_function: prediction {}".format(event["type"], str(event)))
		event["timestamp"] = datetime.datetime.now().isoformat()
		event["now_iso"] = datetime.datetime.now().isoformat()
		if self.publish_proto == KAFKA_PROTO:
			future = self.kafka_producer.send(self.predictions_topic, event)
			try:
				metadata = future.get(timeout=10)
			except KafkaError as ke:
				print('Failed to publish to Kafka topic %s: %s' % (self.predictions_topic, str(ke)))
				raise FunctionException
		if self.publish_proto == MQTT_PROTO:
			msg = jsonlib.dumps(event)
			self.mqtt_client.publish(self.predictions_topic, msg, True)
			self.mqtt_client.loop(2, 10)
	
	def publish_density(self, event):
		print("{}_car_function: density {}".format(event["type"], str(event)))
		event["timestamp"]=datetime.datetime.now().isoformat()
		event["now_iso"]=datetime.datetime.now().isoformat()
		if self.publish_proto == KAFKA_PROTO:
			future=self.kafka_producer.send(self.density_topic, event)
			try:
				metadata=future.get(timeout=10)
			except KafkaError as ke:
				print('Failed to publish to Kafka topic %s: %s' % (self.density_topic, str(ke)))
				raise FunctionException

		if self.publish_proto == MQTT_PROTO:
			msg=jsonlib.dumps(event)
			self.mqtt_client.publish(self.density_topic, msg, True)
			self.mqtt_client.loop(2, 10)


class Repository:

	def __init__(self):
		try:
			self.mongo_url=os.environ['MONGODB_CONNECTION_URL']
		except KeyError:
			self.mongo_url=MONGODB_CONNECTION_URL
		try:
			self.mongo_db=os.environ['MONGODB_DATABASE']
		except KeyError:
			self.mongo_db=MONGODB_DATABASE
		try:
			self.mongo_collection=os.environ['MONGODB_COLLECTION']
		except KeyError:
			self.mongo_collection=MONGODB_COLLECTION

	def connect(self):
		start=datetime.datetime.now()
		print("Connecting to Mongo %s" % (self.mongo_url))
		try:
			self.mongoclient=MongoClient(self.mongo_url, maxPoolSize=500)
			print ("Connected to Mongo %s" % self.mongo_url)
		except Exception as e:
			print('Mongo connection failed %s' % str(e))
			raise FunctionException

		self.db=eval('self.mongoclient.' + self.mongo_db)
		self.collection=eval('self.db.' + self.mongo_collection)
		print("MongoDB connection delay: {}".format(datetime.datetime.now() - start))

	def insert_object(self, obj):
		print("car_function: mongo object " + str(obj))
		obj["timestamp"]=datetime.datetime.now().isoformat()
		car_id=obj['car_id']
		car_type=obj['type']
		try:
			prev_obj=self.collection.find({"car_id": car_id})[0]
			_id=prev_obj["_id"]
			self.collection.remove({"_id": _id})
		except:
			pass
		try:
			res=self.collection.insert_one(obj)
			print("Mongo object #%s inserted ack: %s" %  (res.inserted_id, res.acknowledged))
		except Exception as e:
			print("{}_car_function: Mongo error: {}".format(car_type, e))

	def get_car_count(self, car_id, horizontal_offset, vertical_offset):
		count=self.collection.find({"horizontal_offset": horizontal_offset, "vertical_offset": vertical_offset}).count()
		return count

	def close(self):
		self.mongoclient.close()


class Parser:

	def __init__(self):
		try:
			self.locations_topic=os.environ['MQTT_LOCATIONS_TOPIC']
		except KeyError:
			self.locations_topic=MQTT_LOCATIONS_TOPIC
		try:
			self.future_locations_topic=os.environ['MQTT_FUTURE_LOCATIONS_TOPIC']
		except KeyError:
			self.future_locations_topic=MQTT_FUTURE_LOCATIONS_TOPIC

	def parse(self, event, type):
		try:
			body=event['data']
			topic=body['topic']
			if topic == self.future_locations_topic or topic == self.locations_topic:
			# webhook events come base64 encoded, kafka events are plain JSON
				if 'payload' in body:
					payload=b64decode(body['payload']).decode('utf-8')
					self.payloadObj=jsonlib.loads(payload)
				else:
					self.payloadObj=body
				self.type=self.payloadObj['type']
				if self.type != type:
					raise FunctionException
				self.car_id=self.payloadObj['car_id']
				self.horizontal_offset=self.payloadObj['horizontal_offset']
				self.vertical_offset=self.payloadObj['vertical_offset']
				self.topic=topic
			else:
				print("{}_car_function: Unsupported topic: {}".format(self.type, topic))
				raise FunctionException
		except KeyError:
			raise FunctionException


def execute(parser, queue, repository):

	if parser.topic == parser.future_locations_topic:
		count=0
		if parser.type == "ford":
			count=repository.get_car_count(parser.car_id, parser.horizontal_offset, parser.vertical_offset)
		if parser.type == "toyota":
			count=repository.get_car_count(parser.car_id, parser.horizontal_offset, parser.vertical_offset) / 2
		prediction={"car_id": parser.car_id, "prediction": count, "type": parser.type}
		queue.publish_prediction(prediction)
		print("{}_car_function: prediction published".format(parser.type))

	if parser.topic == parser.locations_topic:
		repository.insert_object(parser.payloadObj)
		print("{}_car_function: location writing".format(parser.type))
		count=repository.get_car_count(parser.car_id, parser.horizontal_offset, parser.vertical_offset)
		density={"car_id": parser.car_id, "density": count, "type": parser.type}
		queue.publish_density(density)
		print("{}_car_function: density published".format(parser.type))


def ford(event, context):
	type="ford"
	print(event['data'])
	try:
		parser=Parser()
		parser.parse(event, type)
		try:
			queue=Queue()
			queue.connect(parser.car_id)
			repository=Repository()
			repository.connect()
			execute(parser, queue, repository)
			repository.close()
			return {"result": "ok"}
		except FunctionException as fe:
			print("Internal error: " + str(fe))
			return {"result": {"error": "Internal error"}}
	except Exception as e:
		print("Unsupported event: " + str(e))
		return {"result": {"warning": "Unsupported event"}}


def toyota(event, context):
	type="toyota"
	try:
		parser=Parser()
		parser.parse(event, type)
		try:
			queue=Queue()
			queue.connect(parser.car_id)
			repository=Repository()
			repository.connect()
			execute(parser, queue, repository)
			repository.close()
			return {"result": "ok"}
		except FunctionException as fe:
			print("Internal error: " + str(fe))
			return {"result": {"error": "Internal error"}}
	except Exception as e:
		print("Unsupported event: " + str(e))
		return {"result": {"warning": "Unsupported event"}}


def mazda(event, context):
	type="mazda"
	try:
		parser=Parser()
		parser.parse(event, type)
		try:
			queue=Queue()
			queue.connect(parser.car_id)
			repository=Repository()
			repository.connect()
			execute(parser, queue, repository)
			repository.close()
			return {"result": "ok"}
		except FunctionException as fe:
			print("Internal error: " + str(fe))
			return {"result": {"error": "Internal error"}}
	except Exception as e:
		print("Unsupported event: " + str(e))
		return {"result": {"warning": "Unsupported event"}}
