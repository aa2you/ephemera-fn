#!/usr/bin/env bash


for fn in ford toyota mazda; do 
	kubeless function delete ${fn}-fn
	kubeless trigger http delete ${fn}
	kubeless trigger kafka delete ${fn}-locations
	kubeless trigger kafka delete ${fn}-future-locations
	kubeless topic delete fn-${fn}-faas-locations
	kubeless topic delete fn-${fn}-faas-future-locations
done
